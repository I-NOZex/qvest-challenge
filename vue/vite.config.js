import { defineConfig } from 'vite'
const { createVuePlugin } = require('vite-plugin-vue2');
const path = require('path')

// https://vitejs.dev/config/
export default defineConfig({
    resolve:{
        alias:{
          '@' : path.resolve(__dirname, './src')
        },
      },    
    plugins: [createVuePlugin()],
})
