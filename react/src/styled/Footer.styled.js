import styled from 'styled-components';

export const Footer = styled.footer`
    box-shadow: 0 0px 23px var(--color-shadow);
    padding: 1.75rem;
    margin: 0 -1.5rem;
    position: fixed;
    bottom: 0;
    width: 100%;
    height: 4.6875rem;
    box-sizing: border-box;
    display: flex;
    justify-content: space-between;
    background-color: var(--color-white);
    z-index: 9999;

    & > * {
        font-weight: 600;
    }
`;
