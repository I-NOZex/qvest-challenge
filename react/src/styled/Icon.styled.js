import styled from 'styled-components';

import svgLocker from '../assets/icons/locker.svg';
import svgCaretDown from '../assets/icons/caret-down.svg';
import svgArrowRight from '../assets/icons/arrow-right.svg';

const directions = {
    down: 0,
    right: 90,
    up: 180,
    left: 270,
};

export const Locker = styled.i`
    width: 24px;
    display: inline-block;
    height: 24px;

    background-color: var(--color-${(props) => props.color ?? 'dark'});

    mask-image: url(${svgLocker});
    mask-size: contain;
    mask-repeat: no-repeat;
`;

export const Caret = styled.i`
    width: 15px;
    display: inline-block;
    height: 10px;

    background-color: var(--color-${(props) => props.color ?? 'dark'});

    mask-image: url(${svgCaretDown});
    mask-size: contain;
    mask-repeat: no-repeat;
    mask-position: bottom center;

    transform: rotate(${(props) => `${directions[props.direction] || 0}deg`});
`;

export const Arrow = styled.i`
    width: 32px;
    display: inline-block;
    height: 18px;

    background-color: var(--color-${(props) => props.color ?? 'dark'});

    mask-image: url(${svgArrowRight});
    mask-size: contain;
    mask-repeat: no-repeat;

    transform: rotate(${(props) => (`${-directions[props.direction] + 90 || 0}deg`)});
`;
