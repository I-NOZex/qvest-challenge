import styled from 'styled-components';

export const Header = styled.header`
    display: grid;
    grid-template-columns: auto 1fr auto;
    position: fixed;
    width: 100%;
    margin: -1.5rem;
    padding: 1.5rem;
    box-sizing: border-box;
    background-color: var(--color-white);
    z-index: 9999;
    align-items: center;
`;

export const Title = styled.span`
    text-align: center;
    font-size: 1.125rem;
    font-weight: 600;
`;
