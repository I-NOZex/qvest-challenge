import styled from 'styled-components';
import * as Icon from './Icon.styled';

export const Dropdown = styled.div`
    display: inline-block;
    outline: none;
    position: relative;
    z-index: 9998;
`;

export const Button = styled.button`
    background: transparent;
    border: none;
    cursor: pointer;
    min-width: 9rem;
    text-align: left;
    font-weight: 600;

    ${Icon.Caret} {
        padding: 0 0.25rem;
    }
`;

export const Options = styled.ul`
    position: absolute;
    list-style: none;
    margin: 0;
    padding: 0.8125rem 1.25rem;
    margin-top: 10px;
    width: max-content;
    background: #FFFFFF;
    border: 1px solid #EEEEEE;
    box-shadow: 2px 2px 4px rgba(42, 45, 43, 0.1);
    border-radius: 4px;    
`;

export const Item = styled.li`
    cursor: pointer;
    color: var(--color-${({ active }) => (active ? 'primary' : 'dark')});
    font-weight: 600;
    padding: 7px;
`;
