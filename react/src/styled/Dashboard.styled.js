import styled from 'styled-components';

export const Dashboard = styled.div`
    display: flex;
    flex-direction: column;
    padding: 7.75rem;

    @media (max-width: 990px) {
        padding: 7.75rem 0;
    }
`;
