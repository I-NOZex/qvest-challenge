import styled from 'styled-components';

import answerActiveIcon from '../assets/icons/answer-active.svg';
import answerInactiveIcon from '../assets/icons/answer-inactive.svg';
import askActiveIcon from '../assets/icons/ask-active.svg';
import askInactiveIcon from '../assets/icons/ask-inactive.svg';

import * as Icon from './Icon.styled';

const cardStatus = {
    LOCKED: {
        icon: askInactiveIcon,
        fgColor: 'var(--color-light)',
        bgColor: 'var(--color-dark)',
    },
    ASK: {
        icon: askActiveIcon,
        fgColor: 'var(--color-text-dark)',
        bgColor: 'var(--color-white)',
    },
    ANSWER: {
        icon: answerActiveIcon,
        fgColor: 'var(--color-text-dark)',
        bgColor: 'var(--color-white)',
    },
    ASKED: {
        icon: askInactiveIcon,
        fgColor: 'var(--color-text-dark)',
        bgColor: 'var(--color-inactive)',
    },
    ANSWERED: {
        icon: answerInactiveIcon,
        fgColor: 'var(--color-text-dark)',
        bgColor: 'var(--color-inactive)',
    },
};

export const CardGrid = styled.section`
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    gap: 1.875rem;

    @media (max-width: 990px) {
        display: flex;
        flex-direction: column;
    }    
`;

export const CardGridHead = styled.header`
    grid-column: 1 / span 3;
    display: grid;
    grid-template-columns: 1fr auto;
`;

export const CardBody = styled.p`
    margin: 0;
    padding-top: 3.375rem;
    padding-bottom: 1.375rem;
    min-height: 138px;
    font-size: 26px;
    flex-grow: 1;
`;

export const CardStatus = styled.span`
    margin-right: 1rem;
`;

export const CardFooter = styled.footer`
    display: flex;
    justify-content: end;
`;

export const CardIcon = styled.span`
    width: 4.0625rem;
    border-radius: 100%;
    height: 4.0625rem;
    width: 4.0625rem;
    background-color: var(--color-light);
`;

export const CardItem = styled.article`
    display: flex;
    flex-direction: column;
    padding: 1.875rem;
    padding-top: 2.5rem;
    padding-left: calc(2px + 1.875rem); // to conteract the 3px yellow border
    background-color: ${(props) => cardStatus[props.status]?.bgColor ?? ''};
    border-radius: 6px;
    border: 1px solid var(--color-gray-light);
    box-sizing: border-box;
    transition: all 300ms;
    cursor: ${(props) => (props.status === 'LOCKED' ? 'default' : 'pointer')};
    box-shadow: 15px 15px 44px rgba(61, 70, 65, 0.15);

    &:hover {
        box-shadow: 15px 15px 44px rgba(61, 70, 65, 0.25);
        transform: scale(1.01);
    }

    ${(props) => ['ASKED', 'ANSWERED'].includes(props.status) && `
        box-shadow: none;
    `};
    
    ${(props) => ['ASK', 'ANSWER'].includes(props.status) && `
        border-left: 3px solid  var(--color-primary);
        padding-left: 1.875rem;
    `}


    ${CardIcon} {
        background-image: url(${(props) => cardStatus[props.status]?.icon ?? ''});
    }

    ${CardBody} {
        color: ${(props) => cardStatus[props.status]?.fgColor ?? ''};
    }

    // I wonder how can I set the colors to each in a collection import
    ${Icon.Locker} {
        background-color: ${(props) => cardStatus[props.status]?.fgColor ?? ''};
    }

    ${Icon.Caret} {
        background-color: ${(props) => cardStatus[props.status]?.fgColor ?? ''};
    }    

    ${CardStatus} {
        color: ${(props) => (props.status === 'LOCKED' ? cardStatus[props.status]?.fgColor : 'var(--color-inactive-text)')};
        font-size: ${(props) => (props.status === 'LOCKED' ? '20px' : '16px')};
        font-weight: ${(props) => (props.status === 'LOCKED' ? '600' : '400')};
        flex-grow: ${(props) => (props.status === 'LOCKED' ? '0' : '1')}
    }
`;
