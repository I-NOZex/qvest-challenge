import styled from 'styled-components';

export const Main = styled.main`
    height: 100%;
    display: grid;
    grid-template-rows: auto 1fr auto;
    padding: 1.5rem;
    box-sizing: border-box;
`;
