import React from 'react';
import * as Styled from '../styled/Header.styled';
import Logo from '../assets/images/qvest-logo.svg';

export default class DropdownMenu extends React.Component {
    render() {
        return (
            <Styled.Header>
                <img alt="QVEST" src={Logo} />
                <Styled.Title>Workplace culture in CRAFT</Styled.Title>
                <span>Closes in 2 days</span>
            </Styled.Header>
        );
    }
}
