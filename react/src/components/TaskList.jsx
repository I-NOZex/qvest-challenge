// eslint-disable-next-line no-unused-vars
import React, { useCallback, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { DateTime } from 'luxon';

import Card from './Card';
import DropdownMenu from './DropdownMenu';

import * as Styled from '../styled/Card.styled';

function TaskList({ tasks }) {
    const [filteredTasks, setFilteredTasks] = useState([]);
    const [countStateRecords, setCountStateRecords] = useState([]);

    // eslint-disable-next-line no-unused-vars
    const onMenuChange = useCallback((itemLabel) => {
        console.log(itemLabel);
        if (itemLabel === '$total') {
            setFilteredTasks(tasks);
        } else {
            const aux = tasks.filter((task) => task.state === itemLabel);
            setFilteredTasks(aux);
        }
    }, [tasks]);

    const relativeDateTime = (dateTime) => {
        if (!dateTime) return '';
        const start = DateTime.local().setLocale('en');
        const end = DateTime.fromSQL(dateTime);
        const diff = start.diff(end);

        return start.minus(diff).toRelativeCalendar();
    };

    const formatDate = (dateTime) => {
        if (!dateTime) return '';
        const date = DateTime.fromSQL(dateTime);

        return date.setLocale('en').toFormat('LLL d, h:ma');
    };

    const generateStatusText = (state, value) => {
        if (!value) return '';
        if (state === 'LOCKED') {
            return `Unlocked ${relativeDateTime(value)}`;
        } if (['ASKED', 'ANSWERED'].includes(state)) {
            return formatDate(value);
        }

        return '';
    };

    useEffect(() => {
        const taskStatusEnum = [
            'LOCKED',
            'ASK',
            'ANSWER',
            'ASKED',
            'ANSWERED',
        ];

        tasks.sort((a, b) => taskStatusEnum.indexOf(a.state) - taskStatusEnum.indexOf(b.state));

        setFilteredTasks(tasks);

        const aux = tasks.reduce((res, curr) => ({
            ...res,
            ...{
                [curr.state]: 1 + (res[curr.state] ?? 0),
                $total: (res.$total ?? 0) + 1,
            },
        }), {});
        setCountStateRecords(aux);
    }, [tasks]);

    return (
        <Styled.CardGrid>
            <Styled.CardGridHead>
                <div>
                    Tasks (
                    {filteredTasks.length}
                    )
                </div>
                <div>
                    <span>Showing</span>
                    <DropdownMenu itemClicked={onMenuChange} filterCounters={countStateRecords} />
                </div>
            </Styled.CardGridHead>

            { filteredTasks.map(
                (task, idx) => (
                    <Card
                        key={`${idx + task.id}`}
                        status={task.state}
                        statusText={generateStatusText(task.state, task.unlockedAt ?? task.answerSentAt ?? task.questionSentAt)}
                        bodyText={task.questionContent ?? task.answerContent}
                    />
                ),
            )}
        </Styled.CardGrid>
    );
}

TaskList.propTypes = {
    tasks: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default TaskList;
