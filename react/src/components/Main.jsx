import React, { useState, useCallback } from 'react';

import Header from './Header';
import Dashboard from './Dashboard';
import Footer from './Footer';

import * as Styled from '../styled/Main.styled';

export default function Main() {
    const [pendingTasks, setPendingTasks] = useState(0);

    const updatePendingTasks = useCallback((tasks) => {
        setPendingTasks(tasks?.filter((t) => t.state === 'ANSWER').length);
    }, []);

    return (
        <Styled.Main>
            <Header />
            <Dashboard onTaskLoad={updatePendingTasks} />
            <Footer pendingTasks={pendingTasks} />
        </Styled.Main>
    );
}
