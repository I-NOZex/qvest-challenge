import React from 'react';

import * as Styled from '../styled/Footer.styled';

// eslint-disable-next-line react/prop-types
export default function Footer({ pendingTasks }) {
    return (
        <Styled.Footer>
            <span>{pendingTasks && `${pendingTasks} pending task`}</span>
            <span>My Activity</span>
        </Styled.Footer>
    );
}
