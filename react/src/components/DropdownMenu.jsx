/* eslint-disable react/prop-types */
import React, { useState, useRef, useEffect } from 'react';
import { CSSTransition } from 'react-transition-group';

import * as Styled from '../styled/Dropdown.styled';
import * as Icon from '../styled/Icon.styled';

function DropdownMenu({ itemClicked, filterCounters }) {
    const [open, setOpen] = useState(false);
    const [activeItem, setActiveItem] = useState('All');
    const dropdownContentNode = useRef(null);
    const dropdownNode = useRef(null);

    useEffect(() => {
        function handleClickOutside(event) {
            if (dropdownNode.current && !dropdownNode.current.contains(event.target)) {
                setOpen(false);
            }
        }

        document.addEventListener('mousedown', handleClickOutside);
        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [dropdownNode]);

    const labels = [
        'All',
        'Locked',
        'Pending',
        'Complete',
    ];

    const labelStatus = {
        All: '$total',
        Locked: 'LOCKED',
        Pending: 'ANSWER',
        Complete: 'ANSWERED',
    };

    const onItemClick = (item) => {
        if (activeItem === item) return;
        setActiveItem(item);
        itemClicked(labelStatus[item]);
        setOpen(false);
    };

    return (
        <Styled.Dropdown ref={dropdownNode}>
            <Styled.Button onClick={() => setOpen(!open)}>
                {`${activeItem} Tasks`}
                <Icon.Caret direction={open ? 'up' : 'down'} color="dark" />
            </Styled.Button>

            <CSSTransition
                in={open}
                timeout={300}
                nodeRef={dropdownContentNode}
                classNames="fade-in-out"
                unmountOnExit
                onEnter={() => setOpen(true)}
                onExited={() => setOpen(false)}
            >

                <Styled.Options ref={dropdownContentNode}>
                    {labels.map((label, idx) => (
                        <Styled.Item active={activeItem === label} onClick={() => onItemClick(label)} key={idx.toString()}>
                            {`${label} (${idx === 0 ? filterCounters.$total : filterCounters[labelStatus[label]]})`}
                        </Styled.Item>
                    ))}
                </Styled.Options>
            </CSSTransition>
        </Styled.Dropdown>
    );
}
export default DropdownMenu;
