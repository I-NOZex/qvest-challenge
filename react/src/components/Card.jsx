import React from 'react';
import PropTypes from 'prop-types';

import * as Styled from '../styled/Card.styled';
import * as Icon from '../styled/Icon.styled';

function Card({ status, statusText, bodyText }) {
    return (
        <Styled.CardItem status={status}>
            <Styled.CardIcon />
            <Styled.CardBody>{bodyText}</Styled.CardBody>
            <Styled.CardFooter>
                {(status === 'LOCKED' && (
                    <>
                        <Styled.CardStatus>{statusText}</Styled.CardStatus>
                        <Icon.Locker />
                    </>
                )) || (
                    <>
                        { ['ASKED', 'ANSWERED'].includes(status) && (
                            <Styled.CardStatus>{statusText}</Styled.CardStatus>
                        )}
                        <Icon.Arrow />
                    </>
                )}
            </Styled.CardFooter>
        </Styled.CardItem>
    );
}

Card.propTypes = {
    status: PropTypes.oneOf(['LOCKED', 'ASK', 'ANSWER', 'ASKED', 'ANSWERED'])
        .isRequired,
    statusText: PropTypes.string,
    bodyText: PropTypes.string,
};

Card.defaultProps = {
    statusText: '',
    bodyText: 'Ask a question',
};

export default Card;
