import React, { useState, useEffect } from 'react';
import TaskList from './TaskList';

import * as Styled from '../styled/Dashboard.styled';

// eslint-disable-next-line react/prop-types
export default function Dashboard({ onTaskLoad }) {
    const [tasks, setTasks] = useState([]);

    useEffect(() => {
        const getData = async () => fetch('data/tasks.json', {
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
            },
        })
            .then((response) => response.json())
            .then((json) => { setTasks(json); onTaskLoad(json); });
        getData();
    });

    return (
        <Styled.Dashboard>
            <h2>Hi Michael Hansen</h2>
            <TaskList tasks={tasks} />
        </Styled.Dashboard>
    );
}
