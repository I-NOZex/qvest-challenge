
### To whom may see this.
This was an interesting challange, pretty smooth development even with my stronger experience with Vue.js rather than React.
I think I might starting to have a soft spot for styled-components... 😏
Eitherway, I'm not too happy with the coupling that the task states end-up having with my dropdown component, but for the sake of pacticality/speed, that how I decided to go.
- The before/after Qvest started flow was not implemented due not being explicit witht eh given requirements.
- The project is acceptablely responsive, the grid converts into a stacked flexbox container under resolutions 990px bellow
- Task cards are shown ordered by it state in the following order: 'LOCKED', 'ASK', 'ANSWER', 'ASKED', 'ANSWERED'.

"Pull, Commit, Push, Repeat" 🤖

----------
----------

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


### `npm test`

### `npm run build`

### `npm run eject`
